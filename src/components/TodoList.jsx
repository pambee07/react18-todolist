import TodoItem from "./TodoItem";

function TodoList(props) {
  const todos = props.todos;

  return (
    <div>
      <h1>My todo list ({todos.length} items):</h1>
      <ul>
        {todos.map(function (task) {
          return (
            <TodoItem
              key={task.id}
              title={task.title}
              completed={task.completed}
            ></TodoItem>
          );
        })}
      </ul>
    </div>
  );
}

TodoList.propTypes = {
  todos: PropTypes.array.isRequired,
};

export default TodoList;
